import { Controller, Get } from '@nestjs/common';
import { ApiProduces } from '@nestjs/swagger';
import appConfig from './config/app.config';

@Controller()
export class AppController {
  @Get()
  @ApiProduces('text/plain')
  getInfo(): string {
    return `${appConfig.name}: ${appConfig.version}`;
  }
}

import { Logger } from '@nestjs/common';

/**
 * Development only logger
 * DO NOT USE IN PRODUCTION
 */
export class DevLogger extends Logger {
  siem(...args) {
    this.log(args.join('|'));
  }

  info(...args) {
    this.log(args.join('|'));
  }
}

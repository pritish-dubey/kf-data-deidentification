import { Controller, Get, Res, HttpException } from '@nestjs/common';
import { ApiTags, ApiProduces } from '@nestjs/swagger';
import appConfig from '../config/app.config';
import devConfig from '../config/dev.config';
import loggerConfig from '../config/logger.config';

@Controller('dev')
@ApiTags('dev')
export class DevController {
  @Get('config')
  @ApiProduces('text/plain')
  getConfig(@Res() response) {
    if (devConfig.getConfigEnabled) {
      const configs = {
        app: appConfig,
        dev: devConfig,
        logger: loggerConfig,
      };
      return response.json(configs);
    }
    throw new HttpException('403 Forbidden', 403);
  }

  @Get('env')
  @ApiProduces('text/plain')
  getEnv(@Res() response) {
    if (devConfig.getEnvEnabled) {
      return response.json({ env: process.env });
    }
    throw new HttpException('403 Forbidden', 403);
  }
}

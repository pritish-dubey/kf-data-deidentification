import { Module } from '@nestjs/common';
import { AppImportsLoader } from './app-imports-loader';
import { TestController } from './test/test.controller';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ...AppImportsLoader.load('config/imports/*.imports.{ts,js}'),
    ScheduleModule.forRoot()
  ],
  controllers: [TestController],
  providers: [],
})
export class AppModule {
}

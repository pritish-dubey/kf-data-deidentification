import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MssoUser, UserService as AuthenticationUserService } from 'ngpd-merceros-authentication-be-components';
import { UserEntity } from './interfaces';

@Injectable()
export class UserService implements AuthenticationUserService {
  constructor(@InjectModel('User') private readonly userModel: Model<UserEntity>) {}

  async findByUsername(username: string): Promise<UserEntity> {
    const result = await this.userModel.findOne({ username }).exec();
    if (result) {
      return result;
    }
    return null;
  }

  async findById(id: number): Promise<UserEntity> {
    const result = await this.userModel.findOne({ globalprofileid: id }).exec();
    if (result) {
      return result;
    }
    return null;
  }

  async save(user: MssoUser): Promise<UserEntity> {
    await this.userModel.updateOne({ globalprofileid: user.globalprofileid }, user, { upsert: true });
    return this.findById(user.globalprofileid);
  }
}

import { Document } from 'mongoose';
import { MssoUser } from 'ngpd-merceros-authentication-be-components';

export interface UserEntity extends MssoUser, Document {
}

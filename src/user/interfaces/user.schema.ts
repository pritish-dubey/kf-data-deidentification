import { Schema } from 'mongoose';

export const UserSchema = new Schema({ // tslint:disable-line: variable-name
  globalprofileid: { type: Number, unique: true },
  username: { type: String, unique: true, set: v => v.toLowerCase() },
  firstName: { type: String },
  lastName: { type: String },
  country: { type: String },
  language: { type: String },
  status: { type: String },
  createdAt: { type: Number, default: 0 },
  updatedAt: { type: Number, default: 0 },
});

UserSchema.virtual('id')
  .get(function userIdGetter() {
    return this._id;
  });

UserSchema.virtual('email')
  .get(function userEmailGetter() {
    return this.username;
  })
  .set(function userEmailSetter(email: string) {
    this.username = email;
  });

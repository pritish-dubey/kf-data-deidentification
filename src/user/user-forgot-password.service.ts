import { Injectable } from '@nestjs/common';
import { Request } from 'express';
import {
  EmailData,
  OnForgotPassword,
  ForgotPasswordEmailPlaceholders,
  ForgotPasswordData,
} from 'ngpd-merceros-authentication-be-components';

@Injectable()
export class UserForgotPasswordService implements OnForgotPassword {
  onPrepareForgotPasswordEmail(defaultEmail: EmailData,
                               placeholders: ForgotPasswordEmailPlaceholders,
                               user: ForgotPasswordData,
                               request: Request): void {
    // apply your email settings here
  }
}

import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  OnRegistration,
  BeforeRegistrationData,
  ActivationEmailPlaceholders,
  EmailData,
} from 'ngpd-merceros-authentication-be-components';
import { Request } from 'express';

@Injectable()
export class UserRegistrationService implements OnRegistration {
  onPrepareActivationEmail(defaultEmail: EmailData,
                           placeholders: ActivationEmailPlaceholders,
                           user: BeforeRegistrationData,
                           request: Request): void {
    // apply your email settings here
  }
}

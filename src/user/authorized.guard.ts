import { Inject, Injectable } from '@nestjs/common';
import { JwtAuthService, MultiAuthenticatedGuard } from 'ngpd-merceros-authentication-be-components';
import authenticationConfig from '../config/authentication.config';

@Injectable()
export class AuthorizedGuard extends MultiAuthenticatedGuard {
  constructor(@Inject(JwtAuthService) jwtAuthService: JwtAuthService) {
    super(authenticationConfig, jwtAuthService);
  }
}

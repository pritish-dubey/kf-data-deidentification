import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { Request } from 'express';
import { AuthorizedGuard } from './authorized.guard';

@ApiTags('user')
@Controller('api/user')
export class UserController {
  constructor(private userService: UserService) {
  }

  @ApiBearerAuth()
  @UseGuards(AuthorizedGuard)
  @ApiOperation({ summary: 'Get current user' })
  @ApiResponse({
    status: 200,
    description: 'current user',
  })
  @ApiResponse({
    status: 500,
    type: Error,
    description: 'Could not get current user',
  })
  @Get('/current')
  async findCurrent(@Req() request: Request) {
    const { user } = (request as any);
    if (!user || !user.globalprofileid) {
      return;
    }
    return await this.userService.findById(user.globalprofileid);
  }
}

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './interfaces/user.schema';
import { UserService } from './user.service';
import { AuthorizedGuard } from './authorized.guard';
import { UserController } from './user.controller';

const userFeature = MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]);

@Module({
  controllers: [UserController],
  imports: [
    userFeature,
  ],
  providers: [UserService, AuthorizedGuard],
  exports: [userFeature],
})
export class UserModule {
}

import { Test, TestingModule } from '@nestjs/testing';

const appConfig = {
  name: '',
  version: '',
};

jest.mock('./config/app.config', () => ({
  __esModule: true,
  default: appConfig,
}));

import { AppController } from './app.controller';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    jest.resetModules();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
    }).compile();

    appController = module.get<AppController>(AppController);
  });

  describe('root', () => {
    appConfig.name = Math.random().toString();
    appConfig.version = Math.random().toString();

    it('should return "app name and version string"', () => {
      expect(appController.getInfo()).toBe(`${appConfig.name}: ${appConfig.version}`);
    });
  });
});

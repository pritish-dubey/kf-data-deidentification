import { Injectable } from '@nestjs/common';
import { Job } from '../batch-jobs/job-interface';
import { jobSchema } from '../batch-jobs/batch-job.schema';
import * as mongoose from 'mongoose'
import { InjectConnection } from '@nestjs/mongoose';

@Injectable()
export class DeidetificationService {


    constructor( @InjectConnection() private readonly connection: mongoose.Connection ){}

    /**
     * Fetch all jobs
     * @returns 
     */
    async getAllJobs(){
        return this.connection.useDb('Data_DeIdentification')
            .model('Job',jobSchema).find({},{dataDump : 0})
    }


    /**
     * add new job
     * @param dataDump 
     * @param filename 
     */
    async addNewJob(dataDump : any , filename : string, user : string, token : string){
        
        let newJob = {
            jobId  : Math.random(),
            completed : false,
            dataDump : dataDump,
            dataDumpId : 200,
            filename : filename,
            uploadedBy : user,
            uploadedTime : new Date(),
            status : 'QUEUD',
            token : token
        }

        return this.connection.useDb('Data_DeIdentification')
            .model('Job',jobSchema).create(newJob)

    }


    /**
     * Placeholder for checkoing data validity
     * @param data 
     * @returns 
     */
    checkDataValidaity(data){
        return true
    }

}
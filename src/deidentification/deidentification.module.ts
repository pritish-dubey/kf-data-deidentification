import { Module } from '@nestjs/common';
import { DeidentificationController } from './deidentification.controller';
import { DeidetificationService } from './deidentification.service';


@Module({
  imports : [],
  controllers: [DeidentificationController],
  providers : [DeidetificationService],
  exports : []
})

export class DeidentificationModule {
}

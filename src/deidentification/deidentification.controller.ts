import { Controller, Get, Res, HttpException, Post, Req, UseInterceptors, UploadedFile, Query, Param } from '@nestjs/common';
import { ApiTags, ApiProduces } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { DeidetificationService } from './deidentification.service';
const fs = require('fs/promises')
const excelToJson = require('convert-excel-to-json');
import { nanoid } from 'nanoid';

@Controller('data-deidentification')
@ApiTags('dev')
@UseInterceptors(FileInterceptor('file'))
export class DeidentificationController {

  private allowedMimeTypes = [
    "application/vnd.ms-excel",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  ]

  constructor(private deIdefService : DeidetificationService){

  }

  @Post('upload')
  @ApiProduces('text/plain')
  async uploadFile(@Req() request, @Res() response, @UploadedFile() file: Express.Multer.File) {

    if( !file || !request.body.user || !this.allowedMimeTypes.includes(file.mimetype) ){
      throw new HttpException('400 Bad Request', 400);
    }

    try {

      // write file
      await fs.writeFile(file.originalname, file.buffer)

      // read file
      const result = excelToJson({
        sourceFile: file.originalname
      });

      let isValid = this.deIdefService.checkDataValidaity(result);

      if(isValid){
        const token = nanoid()

        await this.deIdefService.addNewJob(result,file.originalname,request.body.user,token);

        // delete file 
        await fs.unlink(file.originalname)

        // send resp back
        return response.json({ token : token });

      }
      else{
        throw new HttpException('400 Bad Request. Invalid File', 400);
      }
      
    } catch (error) {
      console.log(error)
      throw new HttpException('500 Internal Server Error', 500);
    }


  }

  @Get('list-jobs')
  @ApiProduces('text/plain')
  async getJobList(@Res() response, @Req() request) {
    return  response.json({  jobs : await this.deIdefService.getAllJobs() });
  }

  @Get('data/:token')
  @ApiProduces('text/plain')
  async getDeidentifiedData(@Res() response, @Req() request, @Param('token') token) {
    if(!token || token == ''){
      throw new HttpException('400 Bad Request', 400);
    }
    return  response.json({  data : { sample : 100 } });
  }

  @Get('reidentified-data/:token')
  @ApiProduces('text/plain')
  async getReidentifiedData(@Res() response, @Req() request, @Param('token') token) {
    if(!token || token == ''){
      throw new HttpException('400 Bad Request', 400);
    }
    return  response.json({  data : { sample : 100 } });
  }
}

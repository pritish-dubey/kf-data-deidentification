import { filterXSS } from 'xss';

export async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

/**
 * @desc trasnform string in camel case
 * @param str
 */
export function camelize(str: string): string {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
    if (+match === 0) return '';
    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
}

export function capitalize(str: string): string {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function setValuesTo(obj: any, value: any) {
  const res = Object.assign({}, obj);
  Object.keys(res).forEach((item) => (res[item] = value));
  return res;
}

/**
 * Transform strings in provided JSON using provided function.
 * Input can be string, array of string, JSON with string properties and so on
 */
export function modify(val: any, func: (arg: any) => void): any {
  if (!val) {
    return val;
  }

  if (typeof val === 'number' || typeof val === 'boolean') {
    return val;
  }

  if (typeof val === 'string') {
    return func(val);
  }

  if (typeof val === 'object') {
    return Array.isArray(val)
      ? val.map((item) => modify(item, func))
      : Object.keys(val).reduce((prev: any, curr: any) => {
          prev[curr] = modify(val[curr], func);
          return prev;
        }, {});
  }

  return val;
}

export function sanitize<T>(val: T): T {
  return modify(val, (value) =>
    filterXSS(value, {
      stripIgnoreTag: true,
      stripIgnoreTagBody: ['script', 'style'],
    }),
  );
}

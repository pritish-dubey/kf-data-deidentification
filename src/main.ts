import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import * as fs from 'fs';
import * as path from 'path';
import helmet from 'helmet';
import { Logger, AllExceptionFilter } from 'ngpd-merceros-logger-be';
import { AppModule } from './app.module';
import appConfig from './config/app.config';
import loggerConfig from './config/logger.config';
import { DevLogger } from './dev/dev.logger';
import { SanitizeInterceptor } from './interceptors/sanitizer.interceptor';

async function bootstrap() {

  const appOptions = {
    bodyParser: false,
  } as any;

  if (appConfig.env === 'dev') {
    appOptions.logger = ['log', 'error', 'warn', 'debug', 'verbose'];
    appOptions.httpsOptions = {
      key: fs.readFileSync(path.resolve(__dirname, '../util/dev-server.key')),
      cert: fs.readFileSync(path.resolve(__dirname, '../util/dev-server.crt')),
    };
  }

  const app = await NestFactory.create(AppModule, appOptions);

  app.use(
    helmet.contentSecurityPolicy({
      useDefaults: true,
    }),
  );

  app.use((request, response, next) => {
    if (request.url === '/api/health-check') {
      return response.send('OK');
    }
    if (request.url === '/api/app-version') {
      return response.send(appConfig.version);
    }
    return next();
  });

  const options = new DocumentBuilder()
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  Logger.init(loggerConfig);

  app.use('/msso', bodyParser.urlencoded({ extended: false }), bodyParser.json());
  app.use('/mfa', bodyParser.urlencoded({ extended: false }), bodyParser.json());
  app.use(cookieParser())
  app.useGlobalInterceptors(new SanitizeInterceptor());
  app.enableCors({
    origin: appConfig.cors,
    credentials: true,
    exposedHeaders: [
      'Accept',
      'authorization',
      'Authentication',
      'Content-Type',
      'If-None-Match',
      'SourceType',
    ],
  });
  //app.useLogger(appConfig.env === 'dev' ? new DevLogger() : new Logger());
  //app.useGlobalFilters(new AllExceptionFilter(app.getHttpAdapter()));
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true,
    validationError: { value: false },
  }));

  console.log(appConfig)
  await app.listen(appConfig.port);
}

bootstrap();

import getenv from 'getenv';

export default {
  env: getenv('NODE_ENV'),
  host: getenv('APP_HOST'),
  frontendHost: getenv('APP_FRONTEND_HOST'),
  port: getenv.int('APP_PORT'),
  name: getenv('APP_NAME', 'main-be'),
  version: getenv('APP_VERSION'),
  cors: getenv.array('APP_CORS')
};

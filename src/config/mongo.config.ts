import getenv from 'getenv';

export default {
  uri: getenv('MONGO_URI'),
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex:true,
  },
};

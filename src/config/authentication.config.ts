import getenv from 'getenv';
import * as os from 'os';
import { AuthenticationOptions } from 'ngpd-merceros-authentication-be-components';
import { DevLogger } from '../dev/dev.logger';
import { Logger } from 'ngpd-merceros-logger-be';
import appConfig from './app.config';
import communicationConfig from './communication.config';

const cert = getenv('AUTHENTICATION_SAML_CERTIFICATE').replace(/\\n/g, os.EOL);
const privateCert = getenv('AUTHENTICATION_SAML_PRIVATE_CERTIFICATE').replace(/\\n/g, os.EOL);

const authenticationConfig: AuthenticationOptions = {
  jwtSecret: getenv('AUTHENTICATION_JWT_SECRET'),
  jwtExpiresIn: getenv.int('AUTHENTICATION_JWT_EXPIRES_IN'),
  jwtRefreshTokenExpiresIn: getenv.int('AUTHENTICATION_JWT_REFRESH_TOKEN_EXPIRES_IN'),
  frontendUrl: getenv('AUTHENTICATION_FRONTEND_URL'),
  loginRedirect: getenv('AUTHENTICATION_LOGIN_REDIRECT'),
  successRedirect: getenv('AUTHENTICATION_SUCCESS_REDIRECT'),
  failureRedirect: getenv('AUTHENTICATION_FAILURE_REDIRECT'),
  accountLockedRedirect: getenv('AUTHENTICATION_ACCOUNT_LOCKED_REDIRECT'),
  passwordExpiredRedirect: getenv('AUTHENTICATION_ACCOUNT_EXPIRED_REDIRECT'),
  jwt: {
    signOptions: {
      audience: getenv('AUTHENTICATION_JWT_SIGN_OPTIONS_AUDIENCE'),
      issuer: 'msso.mercer.com',
    },
  },
  api: {
    app: getenv('AUTHENTICATION_APP_NAME',),
    host: getenv('AUTHENTICATION_MSSO_API_HOST'),
    apiKey: getenv('AUTHENTICATION_MSSO_API_KEY'),
  },
  mfa: {
    enabled: getenv.bool('AUTHENTICATION_MFA_ENABLED', true),
    host: getenv('AUTHENTICATION_MFA_HOST'),
    apiKey: getenv('AUTHENTICATION_MFA_KEY'),
    appScope: getenv('AUTHENTICATION_MFA_SCOPE'),
  },
  saml: {
    entryPoint: getenv('AUTHENTICATION_SAML_ENTRY_POINT'),
    issuer: getenv('AUTHENTICATION_SAML_ISSUER'),
    callbackUrl: getenv('AUTHENTICATION_SAML_CALLBACK_URL'),
    logoutUrl: getenv('AUTHENTICATION_SAML_LOGOUT_URL'),
    logoutCallbackUrl: getenv('AUTHENTICATION_SAML_LOGOUT_CALLBACK_URL'),
  },
  dev: {
    devModeEnabled: getenv.bool('AUTHENTICATION_DEV_MODE_ENABLED', false),
    loginWhitelist: getenv.array('AUTHENTICATION_DEV_LOGIN_WHITELIST', 'string', []),
  },
  mailing: {
    communicationApiUrl: communicationConfig.apiUrl,
    communicationApiKey: communicationConfig.apiKey,
    applicationKey: communicationConfig.applicationKey,
    customEmails: {
      activation: true,
      forgotPassword: true,
      forgotPasswordSuccess: true,
    },
  },
  logger: appConfig.env === 'dev' ? DevLogger : Logger,
  endpoints: {
    dev: getenv.bool('AUTHENTICATION_DEV_MODE_ENABLED', false),
  },
};

if (!authenticationConfig.jwtExpiresIn) {
  delete authenticationConfig.jwtExpiresIn;
}

if (!authenticationConfig.jwtRefreshTokenExpiresIn) {
  delete authenticationConfig.jwtRefreshTokenExpiresIn;
}

if (privateCert) {
  authenticationConfig.saml.privateCert = privateCert;
}

if (cert) {
  authenticationConfig.saml.cert = cert;
}

export default authenticationConfig;

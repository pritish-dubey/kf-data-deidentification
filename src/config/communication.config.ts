import getenv from 'getenv';

export default {
  apiUrl: getenv('COMMUNICATION_API_URL'),
  apiKey: getenv('COMMUNICATION_API_KEY'),
  applicationKey: getenv('COMMUNICATION_API_APP_KEY'),
  applicationName: getenv('COMMUNICATION_API_APP_NAME'),
};

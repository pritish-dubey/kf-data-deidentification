import { AuthenticationModule } from 'ngpd-merceros-authentication-be-components';
import authenticationConfig from '../authentication.config';
import { UserService } from '../../user/user.service';
import { UserModule } from '../../user/user.module';
import { UserRegistrationService } from '../../user/user-registration.service';
import { UserForgotPasswordService } from '../../user/user-forgot-password.service';
import { DeidentificationModule } from '../../deidentification/deidentification.module';
import { BatchJobsModule } from '../../batch-jobs/batch-jobs.module';
import { BatchJobsService } from '../../batch-jobs/batch-jobs.service'

export default AuthenticationModule.forRoot(
  authenticationConfig,
  [UserService, UserRegistrationService, UserForgotPasswordService],
  [UserModule,DeidentificationModule,BatchJobsModule],
);

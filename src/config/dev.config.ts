import getenv from 'getenv';

export default {
  getConfigEnabled: getenv.bool('DEV_GET_CONFIG_ENABLED', false),
  getEnvEnabled: getenv.bool('DEV_GET_ENV_ENABLED', false),
};

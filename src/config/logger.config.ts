import getenv from 'getenv';
import appConfig from './app.config';

export default {
  serviceName: appConfig.name,
  appCode: getenv('LOGGER_APP_CODE'),
  appSubComponent: getenv('LOGGER_APP_SUB_COMPONENT', 'main-be'),
  level: getenv('LOGGER_LEVEL', 'error'),
  json: getenv.bool('LOGGER_JSON', true),
  showLevel: getenv.bool('LOGGER_SHOW_LEVEL', true),
  timestamp: getenv.bool('LOGGER_SHOW_TIMESTAMP', true),
  colorize: getenv.bool('LOGGER_COLORIZE', false),
  coverConsole: getenv.bool('LOGGER_COVER_CONSOLE', true),
  handleExceptions: getenv.bool('LOGGER_HANDLE_EXCEPTIONS', true),
  loggerPrivacyLogRequestBody: getenv.bool('LOGGER_PRIVACY_LOG_REQUEST_BODY', false),
  errorLogBrowserDetailed: getenv.bool('ERROR_LOG_BROWSER_DETAILED', false),
};

import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel, Schema } from '@nestjs/mongoose';
import { Job } from './job-interface';
import { jobSchema } from './batch-job.schema';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose'

@Injectable()
export class BatchJobsService {


    constructor(@InjectConnection() private readonly connection: mongoose.Connection){}

    /**
     * Fetch all jobs
     * @returns 
     */
    async getAllJobs(){
        return this.connection.useDb('Data_DeIdentification').model('Job',jobSchema).find({})
    }


    /**
     * Mark job completed, given job id
     * @param jobId 
     */
    markJobCompleted(jobId : number){

    }

    /**
     * Start push data in diff jobs
     * @param job 
     */
    executeJob(job : Job) : Promise<any>{
        return new Promise((resolve,reject)=>{
            resolve(true)
        })
    }

}
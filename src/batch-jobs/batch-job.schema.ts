import { InjectModel, Schema } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Job } from './job-interface';

export const jobSchema = new mongoose.Schema<Job>({
    jobId : Number,
    filename : String,
    uploadedBy : String, 
    dataDumpId : Number,
    dataDump : mongoose.Schema.Types.Mixed,
    uploadedTime : Date,
    completed : mongoose.Schema.Types.Boolean,
    status : String,
    token : String,
},{ collection : "scheduled-jobs"});
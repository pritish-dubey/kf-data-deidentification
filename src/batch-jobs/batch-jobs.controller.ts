import { Controller, UseInterceptors  } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Cron } from '@nestjs/schedule';
import { BatchJobsService } from './batch-jobs.service';
import getenv from 'getenv';

@Controller('')
@UseInterceptors(FileInterceptor('file'))
export class BatchJobsController {

  
  constructor(private batchService : BatchJobsService){}


  @Cron(getenv('BATCH_CRON'))
  async startJobExecution(){

    console.log('inside cron')
    let jobs = await this.batchService.getAllJobs();

    // if jobs exist
    if(jobs.length){
      for (let index = 0; index < jobs.length; index++) {
        const job = jobs[index];
        console.log('Executing job ',job)
        try {
          let completed = await this.batchService.executeJob(job)

          if(completed){
            // remove task from queue
          }

        } catch (error) {
          // todo : logic when to remove job from queue
        }
        
      }
    }
  }

}

export  interface Job {
    jobId : number,
    filename : string,
    uploadedBy : string, 
    dataDumpId : number,
    dataDump : any,
    uploadedTime : Date,
    completed : boolean,
    status : string,
    token : string
}
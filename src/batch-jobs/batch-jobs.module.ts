import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BatchJobsController } from './batch-jobs.controller';
import { BatchJobsService } from './batch-jobs.service';
import { jobSchema } from '../batch-jobs/batch-job.schema';
import { Job } from './job-interface'
import { UserSchema } from '.././user/interfaces/user.schema'

const jobFeature = MongooseModule.forFeature([{ name: 'Job', schema: jobSchema }]);

@Module({
  imports: [
    jobFeature
  ],
  controllers: [BatchJobsController],
  providers: [BatchJobsService],
  exports : [jobFeature]
})

export class BatchJobsModule {
}

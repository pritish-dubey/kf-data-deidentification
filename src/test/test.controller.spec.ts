import { Test, TestingModule } from '@nestjs/testing';
import { TestController } from './test.controller';

describe('Test Controller', () => {
  let controller: TestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TestController],
    }).compile();

    controller = module.get<TestController>(TestController);
  });

  describe('root', () => {
    it('should return "MainBE is working.."', () => {
      expect(controller.getMessage()).toStrictEqual({"message": "MainBE is working.."});
    });
  });
});

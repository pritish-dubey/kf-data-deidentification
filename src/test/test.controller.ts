import { Controller, Get } from '@nestjs/common';

@Controller('test')
export class TestController {

    @Get()
    getMessage() {
      return {
        message: 'MainBE is working..',
      }
    }
}
